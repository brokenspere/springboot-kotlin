package com.example.demo.repository

import com.example.demo.entity.ShoppinngCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository : CrudRepository<ShoppinngCart,Long> {
    fun findAll(pageable: Pageable):Page<ShoppinngCart>
     fun findBySelectedProduct_Product_NameContainingIgnoreCase(query: String,pageable: Pageable): Page<ShoppinngCart>

    fun findBySelectedProduct_Product_NameContainingIgnoreCase(name:String):List<ShoppinngCart>
}
package com.example.demo.controller


import com.example.demo.entity.Person
import com.example.demo.entity.Person2
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.xml.ws.Response

@RestController
class HelloWorldController{

    @GetMapping("/helloWorld")
    fun getHelloWorld():String{
        return "HelloWorld"
    }
    @GetMapping("/test")
    fun getCamt():String{
        return "CAMT"
    }
    @GetMapping("/person")
    fun getPerson():ResponseEntity<Any>{
        val person = Person("somchai","somrak",15)
        return ResponseEntity.ok(person)
    }
    @GetMapping("/myPerson")
    fun getMyPerson():ResponseEntity<Any>{
        val myperson = Person("nattapat","tamtrakool",20)
        return ResponseEntity.ok(myperson)
    }
    @GetMapping("/persons")
    fun getPersons():ResponseEntity<Any>{
        val person01 = Person("somchai","somrak",15)
        val person02 = Person("Prayut","Chan",62)
        val person03 = Person("Lung","Pom",65)
        val persons = listOf<Person>(person01,person02,person03)
        return ResponseEntity.ok(persons)

    }
    @GetMapping("/myPersons")
    fun getMyPersons():ResponseEntity<Any>{
        val person1 = Person2("Osora","Tsubasa","Nachansu",10)
        val person2 = Person2("Hyuoka","Kojiro","Meiwa",9)

        val persons = listOf<Person2>(person1,person2)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/params")
    fun getParams (@RequestParam("name")name:String,@RequestParam("surname")surname:String)
            :ResponseEntity<Any>{
        return ResponseEntity.ok("$name $surname")
    }

    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParam(@PathVariable("name")name: String,
                     @PathVariable("surname")surname: String,
                     @PathVariable("age")age:Int):ResponseEntity<Any>{
        val person = Person(name,surname,age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo(@RequestBody person: Person) : ResponseEntity<Any>{
        return ResponseEntity.ok(person)
    }
}

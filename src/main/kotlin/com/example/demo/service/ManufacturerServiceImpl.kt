package com.example.demo.service

import com.example.demo.dao.ManufacturerDao
import com.example.demo.dao.ManufacturerDaoImpl
import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl:ManufacturerService{
    @Autowired
    lateinit var manufacturerDao: ManufacturerDao






    override fun getManufacturers(): List<Manufacturer> {

        return manufacturerDao.getManufacturers()
    }
    override fun save(manu: ManufacturerDto):Manufacturer{
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }
}
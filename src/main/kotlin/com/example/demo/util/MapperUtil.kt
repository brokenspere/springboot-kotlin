package com.example.demo.util

import com.example.demo.entity.*
import com.example.demo.entity.dto.*
import com.example.demo.security.entity.Authority
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")

interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }


    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    @Mappings(
            Mapping(source = "manufacturer",target = "manu")
    )
    fun mapProductDto(product: Product?):ProductDto?
    fun mapProductDto (products:List<Product>):List<ProductDto>




    fun mapShopinngCart(shoppinngCart: ShoppinngCart):ShoppingCartDto

    fun mapShopinngCart (shoppinngCart: List<ShoppinngCart>):List<ShoppingCartDto>

    fun mapSelectedProduct (selectedProduct: SelectedProduct):SelectedProductDto
   // fun mapSelectedProduct (selectedProduct: List<SelectedProductDto>):List<SelectedProductDto>

    fun mapSelectedProduct (selectedProduct: List<SelectedProduct>):List<SelectedProductDto>


    @InheritConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @InheritConfiguration
    fun mapAddressDto(address: AddressDto):Address
    fun mapAddressDto(address: Address):AddressDto

    @Mappings(
            Mapping(source = "product.name",target = "name"),
            Mapping(source = "product.description",target = "description"),
            Mapping(source = "quantity",target = "quantity")
    )

    fun mapProductDisplay(selectedProduct: SelectedProduct):DisplayProduct
    fun mapProductDisplay (selectedProduct: List<SelectedProduct>):List<SelectedProduct>

    @Mappings(
            Mapping(source = "customer.name",target = "customer"),
            Mapping(source = "customer.defaultAddress",target = "address"),
            Mapping(source = "selectedProduct",target = "products")
    )

    fun mapShoppingCartCustomerDao(shoppinngCart: ShoppinngCart):ShoppingCustomerDto
    fun mapShoppingCartCustomerDao(shoppinngCart: List<ShoppinngCart>):List<ShoppingCustomerDto>

    @InheritInverseConfiguration
    fun mapProductDto (productDto: ProductDto): Product

    @InheritInverseConfiguration
    fun mapCustomer (customerDto: CustomerDto):Customer

    @Mappings(
            Mapping(source = "customer.jwtUser.username",target = "username"),
            Mapping(source = "customer.jwtUser.authorities",target = "authorities")
    )
    fun mapCustomer (customer: Customer):CustomerDto



    fun mapCustomer(customer:List<Customer> ):List<CustomerDto>
    fun mapAuthority(authority: Authority):AuthorityDto
    fun mapAuthority(authority: List<Authority>):List<Authority>
}
package com.example.demo.service

import com.example.demo.dao.ShoppingCartDao
import com.example.demo.entity.ShoppinngCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl: ShoppingCartService{

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    override fun getShoppingCart(): List<ShoppinngCart> {
        return shoppingCartDao.getShoppingCart()
    }
    override fun getShoppingCart(page:Int,PageSize:Int):Page<ShoppinngCart>{
        return shoppingCartDao.getShoppingCart(page,PageSize)
    }
    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppinngCart> {

        return shoppingCartDao.findByProductName(query,page,pageSize)
    }

}
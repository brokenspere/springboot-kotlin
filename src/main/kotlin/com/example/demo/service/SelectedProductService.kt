package com.example.demo.service

import com.example.demo.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService{

    fun findByProductName(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}
package com.example.demo.entity

import javax.persistence.Entity



enum class ShoppingCardStatus {
    WAIT,CONFIRM,PAID,SENT,RECEIVED
}
package com.example.demo.service

import com.example.demo.entity.Manufacturer
import com.example.demo.entity.dto.ManufacturerDto
import com.example.demo.util.MapperUtil
import org.springframework.stereotype.Service



interface ManufacturerService {
    fun getManufacturers():List<Manufacturer>


    fun save(manu: ManufacturerDto): Manufacturer
}
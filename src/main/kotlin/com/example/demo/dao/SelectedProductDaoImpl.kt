package com.example.demo.dao

import com.example.demo.entity.SelectedProduct
import com.example.demo.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository

class SelectedProductDaoImpl:SelectedProductDao{
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

    override fun findByProductName(name:String,page:Int,pageSize:Int):Page<SelectedProduct>{
        return selectedProductRepository.findByProduct_Name(name,PageRequest.of(page,pageSize))
    }
}
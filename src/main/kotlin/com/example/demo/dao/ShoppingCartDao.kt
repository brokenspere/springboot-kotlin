package com.example.demo.dao

import com.example.demo.entity.ShoppinngCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCart():List<ShoppinngCart>
    fun getShoppingCart(page: Int, pageSize: Int): Page<ShoppinngCart>
    fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppinngCart>
    fun findByProductName(query: String): List<ShoppinngCart>
}
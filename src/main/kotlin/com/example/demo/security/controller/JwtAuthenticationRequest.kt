package com.example.demo.security.controller

data class JwtAuthenticationRequest(var username: String? = null,
                                    var password: String? = null)
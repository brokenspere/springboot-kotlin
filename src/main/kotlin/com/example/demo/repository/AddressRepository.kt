package com.example.demo.repository

import com.example.demo.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository : CrudRepository<Address,Long> {
}
package com.example.demo.service

import com.example.demo.entity.ShoppinngCart
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCart(): List<ShoppinngCart>
    fun getShoppingCart(page: Int, PageSize: Int): Page<ShoppinngCart>
     fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppinngCart>
}
package com.example.demo.service

import com.example.demo.dao.SelectedProductDao
import com.example.demo.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl:SelectedProductService{
    @Autowired
    lateinit var selectedProductDao : SelectedProductDao
    override fun findByProductName(name:String,page:Int,pageSize:Int): Page<SelectedProduct> {
        return selectedProductDao.findByProductName(name,page,pageSize)
    }
}
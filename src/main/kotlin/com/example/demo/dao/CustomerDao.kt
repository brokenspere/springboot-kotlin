package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.ShoppinngCart
import com.example.demo.entity.UserStatus

interface CustomerDao {
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String):Customer ?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvince(address: String): List<Customer>
    fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?

}
package com.example.demo.entity.dto

data class ManufacturerDto (
        var name:String? =null,
        var telNo:String? = null,
        var id:Long? = null
)
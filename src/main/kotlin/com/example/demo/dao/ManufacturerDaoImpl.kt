package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaoImpl:ManufacturerDao{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository

    override fun getManufacturers():List<Manufacturer>{
        return mutableListOf(Manufacturer("Apple","053123456"),
                Manufacturer("Samsung","555666777888"),
                Manufacturer("CAMT","0000000"))
    }
    override  fun save(manufacturer: Manufacturer):Manufacturer{
       return manufacturerRepository.save(manufacturer)
    }

    override fun findById(id:Long):Manufacturer?{
        return manufacturerRepository.findById(id).orElse(null)
    }
}
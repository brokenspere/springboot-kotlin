package com.example.demo.dao

import com.example.demo.entity.Address

interface AdressDao{
    fun save(address: Address): Address
    fun findById(addressId: Long): Address
}
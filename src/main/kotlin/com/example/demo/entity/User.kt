package com.example.demo.entity

import com.example.demo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User ( open var name:String? = null,
                      open var email:String? = null,
                      open var userStatus: UserStatus? = null)

{
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToOne
    var jwtUser:JwtUser? = null
}
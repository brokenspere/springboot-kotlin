package com.example.demo.controller

import com.example.demo.entity.dto.PageShoppingCustomerDto
import com.example.demo.service.ShoppingCartService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService :ShoppingCartService
    @GetMapping("/shoppingcart")

    fun getAllShoppingCart():ResponseEntity<Any>{
        val shoppingcart =shoppingCartService.getShoppingCart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShopinngCart(shoppingcart))
    }


    @GetMapping("/allShoppingCart")
    fun getAllShoppingcart(
            @RequestParam("page")page:Int,
            @RequestParam("pageSize")pageSize:Int):ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCart(page,pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDao(output.content)
        ))
    }
    @GetMapping("/shoppingCart/productName")
    fun getShoppingCart(
            @RequestParam("query")query:String,
            @RequestParam("page")page:Int,
            @RequestParam("pageSize")pageSize:Int):ResponseEntity<Any>{

        val output = shoppingCartService.findByProductName(query,page,pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDao(output.content)
        ))

    }

}
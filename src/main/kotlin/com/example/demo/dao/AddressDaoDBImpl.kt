package com.example.demo.dao

import com.example.demo.entity.Address
import com.example.demo.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository

class AddressDaoDBImpl :AdressDao {
    override fun findById(addressId: Long): Address {
        return addressRepository.findById(addressId).orElse(null)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }
}
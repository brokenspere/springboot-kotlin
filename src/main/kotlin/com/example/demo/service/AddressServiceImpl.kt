package com.example.demo.service

import com.example.demo.dao.AdressDao
import com.example.demo.entity.Address
import com.example.demo.util.MapperUtil
import com.example.demo.entity.dto.AddressDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class  AddressServiceImpl : AddressService{
    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddressDto(address)
        return addressDao.save(address)
    }

    @Autowired
    lateinit var addressDao : AdressDao


}
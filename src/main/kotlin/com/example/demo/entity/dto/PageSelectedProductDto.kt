package com.example.demo.entity.dto

data class PageSelectedProductDto(
        var totalPages: Int? = null,
        var totalElements:Long?= null,
        var  selectedProduct:List<SelectedProductDto> = mutableListOf()

)
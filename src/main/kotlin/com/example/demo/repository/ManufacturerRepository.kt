package com.example.demo.repository

import com.example.demo.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository : CrudRepository<Manufacturer,Long> {
    fun findByName(name:String):Manufacturer
}
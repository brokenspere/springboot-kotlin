package com.example.demo.repository

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository : CrudRepository<Customer,Long> {
    fun findByName(name:String):Customer?
    fun findByNameEndingWith(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, email: String): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(address: String): List<Customer>
    fun findByUserStatus(status: Enum<UserStatus>): List<Customer>
    fun findByIsDeletedIsFalse():List<Customer>
}
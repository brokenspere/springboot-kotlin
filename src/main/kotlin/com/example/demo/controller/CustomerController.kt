package com.example.demo.controller

import com.example.demo.entity.Address
import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.entity.dto.CustomerDto
import com.example.demo.service.CustomerService
import com.example.demo.service.ManufacturerService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController

class CustomerController{
    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getAllCustomers(): ResponseEntity<Any> {
       val Customer = customerService.getCustomers()
        return  ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomer(Customer))
    }

    @GetMapping("/customer/query")
    fun getCustomer(@RequestParam("name")name:String):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByName(name)!!)
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/partialQuery")
    fun getProductPartial(@RequestParam("name")name: String,
                          @RequestParam(value = "email",required = false)email:String?):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByPartialNameAndEmail(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/address")
    fun getCustomerByProvince (@RequestParam("address")address: String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByProvince(address))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerByStatus(@RequestParam("status")status: UserStatus): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomer(
                customerService.getCustomerByStatus(status)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/product")
    fun getCustomerFromBoughtProduct(@RequestParam(name="name")name:String):ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto:CustomerDto):ResponseEntity<Any>{
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomer(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @PostMapping("/customer/address/{addressId}")
    fun addCustomer(@RequestBody customerDto:CustomerDto,@PathVariable addressId:Long):ResponseEntity<Any>{
        val output = customerService.save(addressId,MapperUtil.INSTANCE.mapCustomer(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id")id:Long):ResponseEntity<Any>{
      val customer = customerService.remove(id)
      val outputCustomer = MapperUtil.INSTANCE.mapCustomer(customer!!)
      outputCustomer?.let { return ResponseEntity.ok(it) }
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("customer id not found")
    }


}
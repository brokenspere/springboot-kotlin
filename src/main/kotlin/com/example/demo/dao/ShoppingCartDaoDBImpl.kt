package com.example.demo.dao

import com.example.demo.entity.Product
import com.example.demo.entity.ShoppinngCart
import com.example.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository

class ShoppingCartDaoDBImpl:ShoppingCartDao{

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCart(): List<ShoppinngCart> {
       return shoppingCartRepository.findAll().filterIsInstance(ShoppinngCart::class.java)
    }
    override fun getShoppingCart(page:Int,pageSize:Int):Page<ShoppinngCart>{
        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
    }
    override fun findByProductName(query: String, page: Int, pageSize: Int): Page<ShoppinngCart> {
        return shoppingCartRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(
                query,
                PageRequest.of(page,pageSize)
        )
    }

    override fun findByProductName(query: String):List<ShoppinngCart>{
        return shoppingCartRepository.findBySelectedProduct_Product_NameContainingIgnoreCase(query)
    }

}

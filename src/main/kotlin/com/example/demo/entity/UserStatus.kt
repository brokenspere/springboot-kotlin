package com.example.demo.entity

import javax.persistence.Entity


enum class UserStatus{
    PENDING,ACTIVE,NOACTIVE,DELETED
}
package com.example.demo.service

import com.example.demo.entity.Address
import com.example.demo.entity.dto.AddressDto

interface AddressService{
    fun save(address: AddressDto):Address
}
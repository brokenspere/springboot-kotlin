//package com.example.demo.controller
//
//import com.example.demo.entity.Phone
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//import javax.xml.ws.Response
//
//@RestController
//class  AssessmentController {
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val iphone = Phone("iPhone", "A new telephone", 28000, 5)
//        return ResponseEntity.ok(iphone)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getproductWithPath(@PathVariable("name")name: String): ResponseEntity<Any> {
//        val iphone = Phone("iPhone", "A new telephone", 28000, 5)
//        if(iphone.name.equals(name)){
//            return ResponseEntity.ok(iphone)
//        }else{
//
//            return ResponseEntity.notFound().build()
//        }
//    }
//
////    @GetMapping("/product/iPhoneXs")
////    fun getproductWithPathNotFound(): ResponseEntity<Any> {
////
////        return ResponseEntity.notFound().build()
////    }
//
//    @PostMapping("/setZeroQuantity")
//    fun echo(@RequestBody iphone: Phone): ResponseEntity<Any> {
//        iphone.quantity=0
//        return ResponseEntity.ok(iphone)
//        }
//
//    @PostMapping("/totalPrice")
//    fun totalPrice(@RequestBody iphone: Array<Phone>):String{
//        var total=0
//        for(i in iphone){
//            total = total+ i.price
//        }
//
//        return "$total"
//    }
//    @PostMapping("/avaliableProduct")
//    fun avalable (@RequestBody iphone: List<Phone>):ResponseEntity<Any>{
//        var output  = mutableListOf<Phone>()
//        for(i in iphone){
//            if(i.quantity!=0){
//                output.add(i)
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//}
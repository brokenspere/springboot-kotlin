package com.example.demo.service

import com.example.demo.dao.ManufacturerDao
import com.example.demo.dao.ProductDao
import com.example.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ProductServiceImpl:ProductService{



    @Autowired
    lateinit var productDao: ProductDao

    override fun getProductByName(name: String): Product?
        = productDao.getProductByName(name)

    override fun getProducts(): List<Product> {

        return productDao.getProducts()

    }
    override fun getProductByPartialName(name: String): List<Product> {
            return productDao.getProductByPartialName(name)
    }
    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
            return productDao.getProductByPartialNameAndDesc(name,desc)
    }
    override fun getProductByManuName(name: String): List<Product> {
        return productDao.getProductByManuName(name)
    }
    override fun getProductsWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productDao.getProductsWithPage(name,page,pageSize)
    }
//    override fun save(product: Product): Product {
//        return productDao.save(product)
//    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    @Transactional
    override fun save(manuId:Long,product: Product):Product{
        val manufacturer = product.manufacturer?.let { manufacturerDao.save(it) }
        val product = productDao.save(product)

        manufacturer?.products?.add(product)
        return product
    }
    @Transactional
    override fun remove(id: Long): Product? {
        val product = productDao.findById(id)
        product?.isDeleted = true
        return product
    }
}
package com.example.demo.controller

import com.example.demo.entity.Address
import com.example.demo.entity.dto.AddressDto
import com.example.demo.entity.dto.ManufacturerDto
import com.example.demo.service.AddressService
import com.example.demo.util.MapperUtil

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.*

@RestController

class AddressController {
    @Autowired
    lateinit var addressService: AddressService

    @PostMapping("/address")
    fun addAddress (@RequestBody address:AddressDto):ResponseEntity<Any>{
        val output: Address = addressService.save(address)
        val outputDto: AddressDto = MapperUtil.INSTANCE.mapAddressDto(output)
        return ResponseEntity.ok(outputDto)

    }
    @PutMapping("/address/{addressid}")
    fun updateAddress(@PathVariable("addressid")id:Long?,
                             @RequestBody address: AddressDto):ResponseEntity<Any>{
        address.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddressDto(addressService.save(address)))
    }
}
package com.example.demo.entity.dto

import com.example.demo.entity.Customer
import com.example.demo.entity.ShoppingCardStatus

data  class ShoppingCartDto (
        var status:ShoppingCardStatus = ShoppingCardStatus.WAIT,
        var selectedProduct :List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
        var customer:CustomerDto? = null
)
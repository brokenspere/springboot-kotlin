package com.example.demo.entity

import javax.persistence.*

@Entity
data class Address(var homeAddress: String? =null,
                   var subdistrict: String ? =null,
                   var district: String ? =null,
                   var province: String ? =null,
                   var postCode: String ? =null){

    @Id
    @GeneratedValue
    var id:Long? = null
   // @OneToOne(mappedBy = "shippingadress")
//    @OneToMany
//   var customer = mutableListOf<Customer>()
}
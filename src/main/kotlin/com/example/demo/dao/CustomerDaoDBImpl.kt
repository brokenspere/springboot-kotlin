package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.ShoppinngCart
import com.example.demo.entity.UserStatus
import com.example.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository

class CustomerDaoDBImpl:CustomerDao{



    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }


    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {

        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return  customerRepository.findByName(name)
    }
    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWith(name)
    }
    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }
    override fun getCustomerByProvince(address: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(address)
    }
    override fun save(customer: Customer): Customer {
       return customerRepository.save(customer)
    }

    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }
}
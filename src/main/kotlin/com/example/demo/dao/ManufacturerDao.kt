package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.repository.ManufacturerRepository

interface ManufacturerDao{
   // lateinit var manufacturerRepository: ManufacturerRepository
    fun getManufacturers():List<Manufacturer>

//    override  fun save(manufacturer: Manufacturer):Manufacturer{
//        return manufacturerRepository.save(manufacturer)
//    }

    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(id: Long): Manufacturer?
}
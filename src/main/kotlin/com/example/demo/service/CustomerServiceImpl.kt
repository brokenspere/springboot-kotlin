package com.example.demo.service

import com.example.demo.dao.AdressDao
import com.example.demo.dao.CustomerDao
import com.example.demo.dao.ShoppingCartDao
import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class CustomerServiceImpl:CustomerService{


    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    @Transactional
    override fun getCustomers(): List<Customer> {

        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerDao.getCustomerByName(name)
    }
    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }
    override fun getCustomerByPartialNameAndEmail(name: String,email:String): List<Customer> {
        return  customerDao.getCustomerByPartialNameAndEmail(name,email)
    }
    override fun getCustomerByProvince(address: String): List<Customer> {
        return customerDao.getCustomerByProvince(address)
    }

    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }
    override fun findByBoughtProduct(name: String): List<Customer> {
        return shoppingCartDao.findByProductName(name)
                .mapNotNull { shoppingCart -> shoppingCart.customer}
                .toSet().toList()
    }
    @Autowired
    lateinit var adressDao:AdressDao
    @Transactional
    override fun save(customer: Customer): Customer {
        val address = customer.defaultAddress?.let{adressDao.save(it)}
        val customer = customerDao.save(customer)
      //  address?.customer?.add(customer)
        return customer
    }
    @Transactional
    override  fun save(addressId:Long,customer: Customer):Customer{
        val address = adressDao.findById(addressId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }
    @Transactional
    override fun remove(id:Long):Customer?{
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }
}
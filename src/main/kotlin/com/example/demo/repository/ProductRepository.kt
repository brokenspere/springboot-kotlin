package com.example.demo.repository

import com.example.demo.entity.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.CrudRepository
import org.springframework.data.domain.Pageable

interface ProductRepository : CrudRepository<Product,Long> {
    fun findByName(name:String):Product?
    fun findByNameContainingIgnoreCase(name: String): List<Product>
    fun findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name: String, desc: String): List<Product>
    fun findByManufacturer_NameContainingIgnoreCase(name: String): List<Product>
    fun findByNameContainingIgnoreCase(name: String,page:Pageable):Page<Product>
    fun findByIsDeletedIsFalse():List<Product>
}
package com.example.demo.entity

import javax.persistence.*


@Entity
data class ShoppinngCart(var status: ShoppingCardStatus? = ShoppingCardStatus.WAIT) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var selectedProduct = mutableListOf<SelectedProduct>()

    @OneToOne
    var shippingAddress: Address? = null


    @OneToOne
    var customer: Customer? = null


}
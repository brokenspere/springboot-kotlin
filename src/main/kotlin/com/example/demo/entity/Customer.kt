package com.example.demo.entity

import javax.persistence.*

@Entity
data class Customer (
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus?=UserStatus.PENDING,
        var isDeleted: Boolean = false


) : User (name, email, userStatus){
//    @Id
//    @GeneratedValue
//    var id:Long? = null
    //@OneToOne(mappedBy = "customer")

   // var shoppinngCart = mutableListOf<ShoppinngCart>()

    @ManyToMany
    var shippingAddress = mutableListOf<Address>()

    @ManyToOne
     var billingAddress: Address? = null

    @ManyToOne
     var defaultAddress: Address? = null

}
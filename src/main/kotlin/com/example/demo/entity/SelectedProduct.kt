package com.example.demo.entity

import javax.persistence.*

@Entity
 data class SelectedProduct(var quantity:Int?=null){
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToOne
    lateinit var product: Product
 }
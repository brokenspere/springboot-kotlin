package com.example.demo.service

import com.example.demo.entity.Product
import org.springframework.data.domain.Page
import javax.transaction.Transactional

interface ProductService {
    fun getProducts():List<Product>
    fun getProductByName(name:String): Product?
    fun getProductByPartialName(name: String): List<Product>
     fun getProductByPartialNameAndDesc(name: String, desc: String):List<Product>
     fun getProductByManuName(name: String): List<Product>
     fun getProductsWithPage(name: String, page: Int, pageSize: Int): Page<Product>


    @Transactional
    fun save(manuId: Long, product: Product): Product

     fun remove(id: Long): Product?
}
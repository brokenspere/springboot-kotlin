package com.example.demo.repository

import com.example.demo.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository : CrudRepository<SelectedProduct,Long> {
    fun findByProduct_Name(name:String,pageable: Pageable):Page<SelectedProduct>
}
package com.example.demo.service

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import javax.transaction.Transactional

interface CustomerService {


    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String,email:String): List<Customer>
    fun getCustomerByProvince(address: String): List<Customer>
    fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer>
    fun findByBoughtProduct(name: String): List<Customer>
    fun save(customer: Customer): Customer

    @Transactional
    fun save(addressId: Long, customer: Customer): Customer


    fun remove(id: Long): Customer?
}